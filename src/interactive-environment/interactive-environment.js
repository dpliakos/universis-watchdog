'use strict';

const puppeteer = require('puppeteer');

class InteractiveEnvironment {

  constructor(initiator, target, testFunctionList=[], envConfig = {}) {
    this.target = target;
    this.initiator = initiator;
    this.testFunctionList = testFunctionList;
    this.envConfig = envConfig;
    this.status = 'ready';
    this.error = undefined;
    this.impersonationEntryPoint = `${initiator.oauthServer}/auth/admin/master/console`;
  }

  async initialize() {
    try {
      this.browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
        headless: !(this.envConfig.showBrowser)
      });
      this.page = await this.browser.newPage();
      await this.initiatorLogin();
      this.status = 'initialized';
      await this.impersonateUser();
      this.results = await this.runTestFunctions(this.testFunctionList);
      await this.terminate(this.browser);
      return this.results;
    } catch(err) {
      this.error = 'Error while creating the browser';
      await this.terminate(this.browser);
      throw err;
    }
  }

  async initiatorLogin() {
    if (!this.browser || !this.page) {
      throw new Error('Environment is not initialized');
    }
    try {
      await this.page.goto(this.impersonationEntryPoint, {waitUntil: 'networkidle2'});

      if (this.envConfig.useAUTHImpersonation) {
        await this.page.waitFor('#zocial-AUTH');
        await this.page.click('#zocial-AUTH');
        await this.page.waitForTimeout(1500);
      }
      
      await this.page.waitFor('#username');
      await this.page.waitFor('#password');
      await this.page.type('#username', this.initiator.username);
      await this.page.type('#password', this.initiator.password);

      await this.page.waitForTimeout(100);
      await this.page.click('input[type=submit]');

      await this.page.waitFor('.navbar');
    } catch (err) {
      this.error = 'Error during initiator login';
      this.status = 'error';
      await this.terminate(this.browser, 'Error during initiator login');
      throw err;
    }
  }

  async impersonateUser() {
    try {
      await this.page.goto(`${this.initiator.oauthServer}/auth/admin/master/console/#/realms/master/users`, {waitUntil: 'networkidle2'});
      await this.page.type('input[type=text]', this.target.username);
      await this.page.click('#userSearch');
      await this.page.waitFor('.clip');
      const impersonationButton = await this.page.$x("//td[contains(text(), 'Impersonate')]");
      await impersonationButton[0].click();
      await new Promise( resolve => setTimeout(resolve, 1000));
      await this.page.waitFor('tbody tr td');
      const targetLink = await this.page.$x(`//a[contains(text(), '${this.target.impersonationLabel}')]`);
      await targetLink[0].click();
    } catch(err) {
      this.error = 'Error during impersonation';
      this.status = 'error';
      await this.terminate(this.browser, 'Error during initiator login');
      throw err;
    }
  }

  async runTestFunctions(functionList) {
    const results = [];
    for (let i=0; i<functionList.length; i++) {
      try {
        const testOutput = await functionList[i](this.page, this.target);
        results.push(testOutput);
      } catch(err) {
        if (this.envConfig.debug) {
          console.error(err);
        }
        results[i] = undefined;
      }
    }
    return results;
  }

  /**
   *
   * Cleans up and reports the results.
   *
   * @param browser The puppeteer browser object
   * @param exitCode The exit code to report
   * @param message The message to show
   */
  async terminate(browser, message) {
    try {
      await browser.disconnect();
      await browser.close();
      this.exitMessage = message;
    } catch(e) {
      if (this.envConfig.debug) {
        console.log("[TechnicalError] Could not close the browser");
      }
    } finally {
      this.status = 'terminated';
    }
  }
}

module.exports = {
  InteractiveEnvironment
}
