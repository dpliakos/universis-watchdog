const sipleStudentLogin = async (page, target) => {
  try {
    await page.goto(`${target.service}/#/dashboard`, {waitUntil: 'networkidle2'});
    await page.waitFor('#universis-students');
    return true;
  } catch(err) {
    console.error(err);
    return false;
  }
}

module.exports = {
  sipleStudentLogin
}
